locals {
  environment = "prod"
  production_availability_zones = ["us-east-1a", "us-east-1b"]
}

module "network" {
  source               = "../modules/network"
  environment          = local.environment
  vpc_cidr             = "10.0.0.0/16"
  public_subnets_cidr  = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets_cidr = ["10.0.10.0/24", "10.0.20.0/24"]
  region               = var.aws_region
  availability_zones   = local.production_availability_zones
  key_name             = "production_key"
}

module "ecs" {
  source              = "../modules/ecs"
  environment         = local.environment
  repository_name     = "platform-challenge/${local.environment}"
  vpc_id              = module.network.vpc_id
  availability_zones  = local.production_availability_zones
  subnets             = module.network.private_subnets
  public_subnet       = module.network.public_subnets
  security_group      = module.network.security_group
}
