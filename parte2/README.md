# iti Platform Challenge - PARTE 2

Essa é a solução da parte um do desafio proposto nesse respositório.

A solução prover a possibilidae de deploy de uma aplicação _dockerizada_ em cluster ECS, 
com a possibilidade de utilização de dois ambientes: 

- *dev*: ambiente de desenvolvimento onde é possivel reliza a integração entre diferentes 
frentes de desenvolvimento. A configuração de tal ambiente encontra-se do diretorio *_dev*. 
Todo o código enviado para branchs com nomes que iniciam com *issue-* serão imediatamente 
_deployados_ no ambiente de dev.

- *dev*: ambiente PRODUTIVO, aplicação oferecida aos clientes. A configuração de tal 
ambiente encontra-se do diretorio *_prod*. Todo o código enviado para branch *master* será 
imediatamente _deployado_ no ambiente de prod.

Os ambientes descritos acima utilizam-se dos mesmos modulos para o provisionamento da 
infraestrutura, que estão contidos no diretório **modules**. Qualquer informação que possa 
ser variar de um ambiente para outro deve ser parametrizado no respectivo diretório do 
ambiente no arquivo **main.tf**.


![_dev/main.tf](docs/params-modules-by-env.png)


Embora a estrutura permita a troca um serie de personalizações por ambiente, é extremamente 
recomendado manter os ambientes a mesma interface (parametros), variando apenas as 
informações presentes em _locals_.
Assim os ambientes serão mais próximos uns dos outros.   


## Principais serviços AWS utilziados
  
  - **ECR** para armazenamento de images buildadas.
  - **ECS** para orquestração de containers e com mecanismo de computação **FAGATE**.
  - **CloudWatch** para armazenamento de logs e provisionamento de metas para o **AUTOSCALING** da solução.
  - **S3** para o armazenamento de backend (*.tfstage) do terraform.

## Execução Local

A solução permite a provisionamento por meio de uma execução local para isso é necessário 
atender alguns pré-requisitos:

  - *Terraform* instalado (essa solução foi testada com as versões _0.14.0_ e _0.15.0_)
  - *Credetials AWS* devidamente configuradas.
  - *Bucket S3* para cada ambiente que deseja realizar o provisionamento, sendo configurado no arquivo _terraform.tf_.
  - *Variaveis de Ambiente* requeridas no arquivo *variables.tf* de cada ambiente:
      - *TF_VAR_aws_access_key* = AWS ACCESS KEY ID da conta que provisionará a infra.
      - *TF_VAR_aws_secret_key* = AWS SECRET ACCESS KEY da conta que provisionará a infra.
      - *TF_VAR_aws_region* = AWS REGION que a infra deverá ser provisionada. 

Depois de satisfazer os requisitos acima, os commando terraform podem ser executados a 
partir do diretório do ambiente desejado ou utilizando a opção _-chdir=\<diretorio do ambiente>_.

```

$ terraform -chdir=parte2/_dev init

$ terraform -chdir=parte2/_dev plan

$ terraform -chdir=parte2/_dev apply -auto-approve

``` 

## Configuração Gitlab CI

Para que a pipeline possa ser executada com o Gitlab CI é necessária a configuração das 
seguintes variáveis no projeto:

![_dev/main.tf](docs/configure-variables-CICD.png)

O nomes são auto descritivos e fazem a configuração de acesso a AWS. Além disso, os buckets de 
state para cada ambiente deve ser criado manualmente, assim como no tópico anterior. 


**Atenção**

A stage de build da pipe tem uma dependência com o repositório ECR para fazer o upload da image. 
Assim antes de executar a pipeline, commente a linha de execução do push no build ou exeute localmente
o terraform.  