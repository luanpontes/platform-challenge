# iti Platform Challenge - PARTE 1

Essa é a solução da parte um do desafio proposto nesse respositório.

A solução prover a possibilidae de deploy de uma aplicação _dockerizada_ em cluster K8s, 
utilizando HELM na Google Could. A escolha da Google Could se dá pela facilidade de provisionamento 
de um cluster k8s que ela oferece.  

A implementação neste diretório não é funcional, ele é um ensaio do que eu acredito que deve ser feito
para a criação e instalação de um pacote helm. Esse é escopo que consegui entregar dentro do tempo 
disponilibizado para o desafio e da minha organização. Agradeço por chamarem minha atenção a essa ferramenta,
me parece promissora e continuarei meus estudos sobre ela.  
