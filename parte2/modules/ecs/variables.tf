variable "environment" {
  description = "The environment"
}

variable "vpc_id" {
  description = "The VPC id"
}

variable "availability_zones" {
  type        = list(string)
  description = "The azs to use"
}

variable "repository_name" {
  description = "The name of the repisitory"
}
variable "web_cpu_resource" {
  description = "The amount cpu for web task definition"
  default = 256
}

variable "web_memory_resource" {
  description = "The amount cpu for web task definition"
  default = 512
}
variable "subnets" {
  type        = list
  description = "The private subnets to use"
}

variable "public_subnet" {
  type        = list
  description = "The public subnets to use"
}

variable "security_group" {
  description = "The security group for network config ecs service"
}
