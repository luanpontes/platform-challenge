

Iniciei a solução pela parte 2 que eu considero ter melhor conhecimento. 

## Problema inicial.

Baixando o projeto e tentando executar o build gradle localmente: 

![problema build app](imgs/problema1.png)

Solução

Alterar a versão do kotlin (ext.kotlin_version) de  = '1.2.61' para '1.4.30'.

O problema era causado pela versão do meu gradle, mas maner a mantive a versão original do projeto 
e coloquei o gradle na versão 4.10 como especificado.  

## Parte 2

### 1 - Configurações inicais do Terraform 

Desde o inicio optei por utilizar um stage em um bucket S3, que possibilita 
o compartilhamento do state da infra entre a pipeline e os desenvolvedores. 
Em alguns casos pode ser necessário executar o provisionamento de uma maquina local,
podendo ficar a cargo da configuração de acesso ao bucket a definição de quem pode executar 
o provisionamento. 

A maior parte das implementações relacionadas a configuração do Terraform estão aqui branch _configure-terraform_.

### 2 - Criação de modulos 

Optei pela criação de dois modulos para segregar alguns dos resources terraform que deveria criar. 
O molulos permite um melhor aproveitamento de código e centraliza as diferenças entre ambientes no 
parametros que sao pasados a cada ambiente. 

A maior parte das inplementações relacionadas a criação de modulos estão aqui branch _create-modules_.

### 3 - Configuração de CI 

Depois dos modulos provisionando infra localmente, hora de configurar a automação para build e provisionamento. 
Primeiramente dockerizei a aplicação usando multstage no Dockerfile, onde o primeiro stage builda a aplicação e 
a segunda é o ambiente de execução da aplicação. Gosto dessa abordagem, pois ela empodera o desevolvedor que 
ganha mais controle tanto sobre o build da sua aplicação (ele é a pessoa que melhor pode especificar como a app
deve se _"buildada"_), como em como a app deve ser executada. Claro que em alguns lugares, por alguns motivos
esse "poder" não deve ser dado aos devs.

A maior parte das inplementações relacionadas a configuração de CI estão aqui branch _ci-configuration_.

### 3 - Configuração do Deploy

Nessa etapa foi configurado o Gitlab Runner em tempo de execução para executar o terraform e realizar o deploy.
Talvez nessa etapa eu deveria ter optado por configar um runner personalizado e utiliza-lo na minha pipe, isso
otimizaria cada execução. Talvez essa seja uma melhoria que poderia ter sido implementada. 


A maior parte das inplementações relacionadas a configuração de CI estão aqui branch _deploy-configuration_.

### 4 - Implementação de porvisonamento por ambientes

A maioria das pipelines que conheço entregam uma aplicação em mais de um ambiente. Nesso eu adicionei essa feature
a essa pipeline tentando deixa-la mais simples possível e evitando ao maximo a duplicação de código. A estrategia de 
modulos do Terraform ajudou bastante, além disso, criar uma pasta para cada ambiente com tudo que pode ser parametrizado
entre eles foi a solução mais simples que encontrei, inclusive parece ser uma sugestao da Hashcorp. (link)

Tudo que é comitado na master vai para produção. E as branchs de desevolvimento iniciam com o prefixo _'issue-'_, acho 
mais interessante que ter uma branch fixa de develop. Uma branch fixa para um ambiente comumente leva a pratica de _cherry-pick_,
ou sempre elevam o rico de um código entrar em produção acidentalmente. Isso pode acontecer porque no momento do merge 
(ex.: develop > master) um codigo de uma feature não homologada poderia estar em develop ou alguém esqueceu de tirar TUDO 
que deveria.  Além disso, nada impede que tenha varias _features branchs_ combinadas em uma brach de issue formando uma especie
de release (ex.: _issue-7893and7897_, um desenvolvedor teria a _issue-7893_ e outro a _issue-7897_, fariam o merge para colocar 
juntos suas funcionalidades em desenvolvimento), talvez o prefixo poderia ser _release-_ ao invés de _issue-_. 

Com essa abordagem abordagem que adotei, acredito que entrego mais agilidade e flexibilidade, mas conto com uma boa comunicação
no time de desenvolvimento e o minimo de organização. 

A maior parte das inplementações relacionadas a configuração de CI estão aqui branch _configue-envs_.

### 5 - Testes 

Os tetes de execução da estratégia acima forma feitos nas branchs _issue-test123_ e _master_.


