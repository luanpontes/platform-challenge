output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "public_subnets" {
  value = aws_subnet.public_subnet
}

output "private_subnets" {
  value = aws_subnet.private_subnet
}

output "default_sg_id" {
  value = aws_security_group.default.id
}

output "security_group" {
  value = aws_security_group.default
}