/*====
ECR repository to store our Docker images
======*/
resource "aws_ecr_repository" "ecr_repository" {
  name = var.repository_name
}

/*====
ECS cluster
======*/
resource "aws_ecs_cluster" "cluster" {
  name = "${var.environment}-platform-challenge"
}


/*====
Cloudwatch Log Group
======*/
resource "aws_cloudwatch_log_group" "platform-challenge-cw" {
  name = "platform-challenge"
}

/*====
Service Web
======*/

resource "aws_ecs_service" "web" {
  name            = "${var.environment}-web"
  task_definition = "${aws_ecs_task_definition.web.family}:${max(aws_ecs_task_definition.web.revision, data.aws_ecs_task_definition.web.revision)}"
  desired_count   = 2
  launch_type     = "FARGATE"
  cluster         = aws_ecs_cluster.cluster.id
  depends_on      = [aws_iam_role_policy.ecs_service_role_policy]

  network_configuration {
    security_groups = [var.security_group.id, aws_security_group.ecs_service_sg.id]
    subnets         = var.subnets.*.id
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.alb_target_group.arn
    container_name   = "web"
    container_port   = "80"
  }

}

/*====
ECS task definitions
======*/

/* the task definition for the task_definition web service */
data "template_file" "web_task" {
  template = file("${path.module}/task/web_task.json")

  vars = {
    image           = aws_ecr_repository.ecr_repository.repository_url
    log_group       = aws_cloudwatch_log_group.platform-challenge-cw.name
  }
}

resource "aws_ecs_task_definition" "web" {
  family                   = "platform-challenge_${var.environment}_web"
  container_definitions    = data.template_file.web_task.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.web_cpu_resource
  memory                   = var.web_memory_resource
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_execution_role.arn
}