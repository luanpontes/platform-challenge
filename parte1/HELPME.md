
https://cloud.google.com/artifact-registry/docs/helm/quickstart

### LOGIN

gcloud container clusters get-credentials autopilot-cluster-1 --zone us-central1 --project iti-platform-challenge 

### Publish CHAR

1. Salva o char platform-challenge com a "tag" us-central1-docker.pkg.dev/iti-platform-challenge/helm-repo/platform-challenge

HELM_EXPERIMENTAL_OCI=1 helm chart save platform-challenge us-central1-docker.pkg.dev/iti-platform-challenge/helm-repo/platform-challenge

2. Veririca se o char foi salvo

HELM_EXPERIMENTAL_OCI=1 helm chart list

3. Configura o registry para o helm

HELM_EXPERIMENTAL_OCI=1 gcloud auth print-access-token | HELM_EXPERIMENTAL_OCI=1 helm registry login -u oauth2accesstoken --password-stdin https://us-central1-docker.pkg.dev

4. Manda o char para o repositório

HELM_EXPERIMENTAL_OCI=1 helm chart push us-central1-docker.pkg.dev/iti-platform-challenge/helm-repo/platform-challenge:0.1.1

5. Verifica se o char foi publicado no repositorio.

HELM_EXPERIMENTAL_OCI=1 gcloud artifacts docker images list us-central1-docker.pkg.dev/iti-platform-challenge/helm-repo/

### Deploy image

1. Remove char localmente 

HELM_EXPERIMENTAL_OCI=1 helm chart remove us-central1-docker.pkg.dev/iti-platform-challenge/helm-repo/platform-challenge:0.1.1

2. Pegar image repositorio

HELM_EXPERIMENTAL_OCI=1 helm chart pull us-central1-docker.pkg.dev/iti-platform-challenge/helm-repo/platform-challenge:0.1.1

3. Verifica se foi baixado

HELM_EXPERIMENTAL_OCI=1 helm chart list

4. Extrai a image 

HELM_EXPERIMENTAL_OCI=1 helm chart export us-central1-docker.pkg.dev/iti-platform-challenge/helm-repo/platform-challenge:0.1.1

5. Instala chart 

helm install hello-chart ./hello-chart

#### Esses substitutions, replace variaveis de dentro do arquivo.

gcloud builds submit . --config=cloudbuild.yaml --substitutions BRANCH_NAME=$CI_COMMIT_REF_NAME

### List all images cluster 

kubectl get pods --all-namespaces -o jsonpath="{..image}" |\
tr -s '[[:space:]]' '\n' |\
sort |\
uniq -c

Começando com helm da google.

https://cloud.google.com/artifact-registry/docs/helm/quickstart#gcloud

Commandos uteis Kubernetes

https://kubernetes.io/pt/docs/reference/kubectl/cheatsheet/