
/*====
Load Balancer
======*/
resource "aws_alb" "alb_platform-challenge" {
  name            = "${var.environment}-alb-challenge"
  subnets         = var.public_subnet.*.id
  security_groups = [aws_security_group.alb_web_inbound_sg.id]
}

resource "aws_alb_listener" "platform-challenge-listener" {
  load_balancer_arn = aws_alb.alb_platform-challenge.arn
  port              = "80" // load balance port listner
  protocol          = "HTTP"
  depends_on        = [aws_alb_target_group.alb_target_group]

  default_action {
    target_group_arn = aws_alb_target_group.alb_target_group.arn
    type             = "forward"
  }
}

resource "random_id" "target_group_sufix" {
  byte_length = 2
}
resource "aws_alb_target_group" "alb_target_group" {
  name     = "${var.environment}-alb-target-group-${random_id.target_group_sufix.hex}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  target_type = "ip"

  lifecycle {
    create_before_destroy = true
  }
  
  health_check {
    path = "/"
    port = 80
  }
}