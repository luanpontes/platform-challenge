terraform {
  backend "s3" {
    bucket  = "backend-platform-challenge-dev"
    key     = "terraform.tfstate"
    region  = "us-east-1"
    profile = "default"
  }
}