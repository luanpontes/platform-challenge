terraform {
  backend "s3" {
    bucket  = "backend-platform-challenge-prod"
    key     = "terraform.tfstate"
    region  = "us-east-1"
    profile = "default"
  }
}